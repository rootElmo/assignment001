# Assignment001

This is the first lesson task in the **CPP Embedded Development Bootcamp**.

The purpose of creating and upkeeping this repository is to show that I know the basics of using **Git**.

![catears](./cat_ears.png)

![earl](./earl_the_cat.jpg)
